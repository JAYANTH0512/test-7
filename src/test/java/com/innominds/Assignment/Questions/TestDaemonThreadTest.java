package com.innominds.Assignment.Questions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestDaemonThreadTest {
	
	TestDaemonThread t1 = new TestDaemonThread();

	@Test
	void testdaemon() {
		
		
		 t1.setDaemon(true);
		    assertEquals(false,TestDaemonThread.currentThread().isDaemon());
		    assertEquals(true, t1.isDaemon());
		
	}

}
