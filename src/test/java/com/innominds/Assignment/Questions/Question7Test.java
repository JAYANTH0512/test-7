package com.innominds.Assignment.Questions;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question7Test {
	
	
	
	@Test
	public void nonstatic() {
		Question7 q7 = new Question7();
		
		assertNotEquals(3,q7.countNonStatic);
		
	}
	
	@Test
	public void statictest() {
		Question7 p2 = new Question7();
		
		assertEquals(5,p2.countstatic);
	}
	
}
