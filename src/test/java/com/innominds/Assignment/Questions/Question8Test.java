package com.innominds.Assignment.Questions;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class Question8Test {
	
	Question8 q8 = new Question8();

	@Test
   public void testSaveEvenNumbers() {
        List<Integer> expectedList = Arrays.asList(2, 4, 6, 8, 10);
        assertArrayEquals(expectedList.toArray(), q8.saveEvenNumbers(10).toArray());



   }



   @Test
    public void testPrintEvenNumbers() {
        ArrayList<Integer> inputList = q8.saveEvenNumbers(10);
        List<Integer> resultList = Arrays.asList(4, 8, 12, 16, 20);
        assertArrayEquals(resultList.toArray(), q8.printEvenNumbers(inputList).toArray());
    }
}


