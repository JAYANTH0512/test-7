package com.innominds.Assignment.Questions;
/**
 * 8.Write unit tests for a program to create a array list of integer having some values.
 *  List those and get the even numbers using stream API. 
 */

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Question2 {
		
	public static  List<Integer> evennumbers() {
	
	List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);

	return numbers.stream().filter(value -> value % 2 == 0).collect(Collectors.toList());
	
	}

}
