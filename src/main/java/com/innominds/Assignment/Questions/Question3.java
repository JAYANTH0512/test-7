package com.innominds.Assignment.Questions;


/**
 * 6.Write unit tests for prepare addition of two numbers method and implement
 * the same using lambda expression
 * 
 * @author jpalepally1
 *
 */

interface Arithmetic {
	public int operations(int c, int d);
}

public class Question3 {

//public static BiFunction<Integer , Integer,Integer> add = (a,b)->a+b;

	public static int add(int c,int d) {
		 Arithmetic addition = (a, b) -> (a + b);
		return addition.operations(c,d);
	}
	
//	

}
