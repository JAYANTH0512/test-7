package com.innominds.Assignment.Questions;

/**
 * 6.Write unit tests for:
     Develop a java class with a method saveEvenNumbers(int N) using ArrayList to store even numbers from 2 to N, where N is a integer which is passed as a parameter to the method saveEvenNumbers().
     The method should return the ArrayList (A1) created.  
 */

import java.util.ArrayList;

public class Question8 {
	   ArrayList<Integer> Arrays = new ArrayList<Integer>();
	  public ArrayList<Integer> saveEvenNumbers(int N) {
	    
		  Arrays = new ArrayList<Integer>();
	            for (int i = 2; i <= N; i++) {
	                if (i % 2 == 0)
	                	Arrays.add(i);
	            }
	        
	        return Arrays;
	}
	   public ArrayList<Integer> printEvenNumbers(ArrayList<Integer> Arrays) {
	        ArrayList<Integer> Arrays2 = new ArrayList<Integer>();
	        for (int item : Arrays) {
	        	Arrays2.add(item * 2);

	       }
	       return Arrays2;
	   }

	}


