package com.innominds.Assignment.Questions;


import java.util.ArrayList;
import java.util.List;

/**
 * Write unit test for a program to find the duplicate characters in a string. 
 * @author jpalepally1
 *
 */

public class Question9 {
	
	public static List<Character> duplicatechar() {
	      String str = "swapna";
	      List<Character> list = new ArrayList<>();
	      
	      char[] carray = str.toCharArray();
	      System.out.println("The string is:" + str);
	      System.out.print("Duplicate Characters in above string are: ");
	      for (int i = 0; i < str.length(); i++) {
	         for (int j = i + 1; j < str.length(); j++) {
	            if (carray[i] == carray[j]) {
	               System.out.print(carray[j] + " ");
	               list.add(carray[j]);
	               break;
	            }
	         }
	      }
		return list;		
      }
	
}
