package com.innominds.Assignment.Questions;

import java.time.LocalDate;
import java.time.Period;

public class Question6 {
	
	public String findDifference(LocalDate start_date, LocalDate end_date)       
    {
        Period diff= Period .between(start_date, end_date);
        System.out.print( "Difference " + "between two dates is");
        System.out.printf( "%d years, %d months" + " and %d days ",diff.getYears(),diff.getMonths(), diff.getDays());
        String d=diff.getYears()+","+diff.getMonths()+","+diff.getDays();
       return d;
                       
    }
}


